'Administration' story

Narrative:
In order to check 'Ebay Search' feature
As a ebay 'Customer' user
I want to type some text and make sure we have the results

Scenario: Dealer Portal Smoke -> Add new user as 'End user' to DP profile
Meta: @kara

Given The 'EBAY Login' page is opened
When I type 'test1' to the 'Search for anything' field
And I click on the 'Search' button and open 'EBAY Search' page
Then The element '1st Product Title' contains text 'Olympus Labs'
