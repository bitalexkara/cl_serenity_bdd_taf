package ebay.pages.login;

import core.pages_with_steps_definitions.BasePage;
import core.utils.driver.DriverUtils;
import core.utils.reflection.annotations.ButtonLink;
import core.utils.reflection.annotations.PageName;
import core.utils.reflection.annotations.TextField;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

@PageName("EBAY Login")
public class EbayLoginPage extends BasePage {
    @TextField(Controls.SEARCH_TEXTFIELD)
    private String searchTextField = ".//*[@id='gh-ac']";

    @ButtonLink(Controls.SEARCH_BUTTON)
    private String buttonSubmit = ".//*[@id='gh-btn']";

    public EbayLoginPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void verify(){
        DriverUtils.waitForPageLoad(getDriver());
        $(searchTextField, 15, TimeUnit.SECONDS);
    }

    public class Controls{
        static final String SEARCH_TEXTFIELD = "Search for anything";
        static final String SEARCH_BUTTON = "Search";
    }
}