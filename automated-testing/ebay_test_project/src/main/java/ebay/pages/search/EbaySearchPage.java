package ebay.pages.search;

import core.pages_with_steps_definitions.BasePage;
import core.utils.driver.DriverUtils;
import core.utils.reflection.annotations.PageName;
import core.utils.reflection.annotations.TextField;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

@PageName("EBAY Search")
public class EbaySearchPage extends BasePage {
    @TextField("1st Product Title")
    private String firstProductTitle = "//*[@id='srp-river-results']/ul/li[1]//h3";

    public EbaySearchPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void verify(){
        DriverUtils.waitForPageLoad(getDriver());
        $(firstProductTitle, 15, TimeUnit.SECONDS);
    }
}
