package core.entities;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExampleMap extends HashMap <String, String> {

	public ExampleMap() {}	
	
	public ExampleMap(Map<String, String> userData) {	
		this.putAll(userData);
	}
	
	/**
	 * This function compares fields of this instance with other object of the same class that is passed as input parameter
	 * The difference is stored in String that will contain field name, actual data (this) and expected data (other)
	 * 
	 * @param fieldValueMap for comparison
	 * @param ignoreFields which fields to ignore. Passing "null" means that all fields must be compared
	 * 
	 * @return string with values that are different
	 */
	public String getDiffLogs (ExampleMap fieldValueMap, List<String> ignoreFields) {
		StringBuilder allDiffLogs = new StringBuilder();

		for (Map.Entry<String, String> expectedEntry : fieldValueMap.entrySet()) {

			String fieldName = expectedEntry.getKey();
			String expectedFieldValue = expectedEntry.getValue();
			
			//check if the field_name is in ignoreKeys list. If yes - skip the value
			if (ignoreFields!=null && ignoreFields.contains(fieldName))
				continue;
			
			if (!this.containsKey(fieldName)) {
				allDiffLogs.append(createDiffMessage(fieldName, " ", expectedFieldValue));
			}
			else {
				String actual_field_value = this.get(fieldName);
				if (!actual_field_value.equalsIgnoreCase(expectedFieldValue)){
					allDiffLogs.append(createDiffMessage(fieldName, actual_field_value, expectedFieldValue));
				}
			}
		}

		return allDiffLogs.toString();
	}

	public String createDiffMessage(String fieldName, String actualVal, String expedtedVal) {

		return String.format("Field: %s; Actual: %s; Expected: %s", fieldName, actualVal, expedtedVal);
	}
}