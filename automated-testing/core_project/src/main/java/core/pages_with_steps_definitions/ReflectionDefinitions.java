package core.pages_with_steps_definitions;

import core.utils.EnvironmentUtils;
import core.utils.reflection.annotations.TextField;
import net.thucydides.core.annotations.Steps;
import org.apache.commons.lang.NotImplementedException;
import org.hamcrest.Matchers;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsString;

public class ReflectionDefinitions {
	@Steps
	BaseSteps baseSteps;
	@Steps
	ReflectionSteps reflectionSteps;

	@Given("The '$pageName' page is opened")
    @When("I open the '$pageName' page")
	public void ThePageIsOpened(String pageName){
        reflectionSteps.openAndVerifyPage(pageName);
	}

	@When("I click on the '$buttonLinkName' {link|button}")
	public void whenIClickOnTheButton(String buttonLinkName){
		reflectionSteps.clickOnTheButtonOrLink(buttonLinkName);
	}

	@When("I click on the '$buttonLinkName' {link|button} and open '$pageName' page")
	public void whenIClickOnTheButtonOrLinkAndOpenAnotherPage(String buttonLinkName, String pageName){
		reflectionSteps.clickOnTheButtonOrLinkAndVerifyPage(buttonLinkName, pageName);
	}

	@When("I click on the '$buttonLinkName' {link|button} and open new '$windowName' window")
	public void whenIClickOnTheButtonOrLinkAndOpenNewWindow(String buttonLinkName, String windowName){
		reflectionSteps.clickOnTheButtonOrLinkWithNewWindowOpened(buttonLinkName, windowName);
	}
	
	@When("I select the '$radioButtonName' radio button value")
	public void whenISelectTheRadioButton(String radioButtonName){
		reflectionSteps.selectRadioButton(radioButtonName);
	}

	@When("I select the '$checkBoxName' checkbox")
	public void whenISelectTheСheckBox(String checkBoxName){
		reflectionSteps.selectCheckBox(checkBoxName, true);
	}

	@When("I unselect the '$checkBoxName' checkbox")
	public void whenIUnSelectTheСheckBox(String checkBoxName){
		reflectionSteps.selectCheckBox(checkBoxName, false);
	}

	@When("I type '$value' to the '$fieldName' field")
	public void whenITypeValueToTheField(String value, String fieldName) throws Exception {
		value = EnvironmentUtils.getEnvironmentDependentValue(value);

		reflectionSteps.typeValueToTheField(value, fieldName);
	}

	@When("I select '$value' from the '$dropdownName' dropdown")
	public void whenISelectFromDropdown(String value, String dropdownName) throws Exception {
		value = EnvironmentUtils.getEnvironmentDependentValue(value);

		reflectionSteps.selectValueFromDropDown(value, dropdownName);
	}

	@Then("The '$textFieldName' text field is not empty")
    public void theTextFieldIsNotEmpty(String textFieldName) throws Exception {
        textFieldName = EnvironmentUtils.getEnvironmentDependentValue(textFieldName);
        String textValue = reflectionSteps.getControlText(textFieldName, TextField.class);

        Assert.assertTrue(String.format("The '%s' text value is empty", textValue), !textValue.isEmpty());
    }

	@Then("The element '$controlName' contains text '$value'")
	public void theElementContainsText(String controlName, String value){
		String controlText = reflectionSteps.getControlText(controlName);
		Assert.assertThat(controlText, containsString(value));
	}

	@Then("The new window '$windowName' is opened")
    public void theNewWindowIsOpened(String windowName){
        reflectionSteps.verifyOpenedPage(windowName);
    }

    @Then("The checkbox '$chkBoxName' is '$checkedUnchecked'")
	public void checkCheckBoxCurrentState(String chkBoxName, String chkBoxState){
		if(chkBoxState.equalsIgnoreCase("checked"))
			Assert.assertThat("Expecting checkbox to be checked", reflectionSteps.isCheckBoxSelected(chkBoxName), is(true));
		else if(chkBoxState.equalsIgnoreCase("unchecked"))
			Assert.assertThat("Expecting checkbox to be unchecked", reflectionSteps.isCheckBoxSelected(chkBoxName), is(false));
		else
			throw new NotImplementedException("Method checks for checked or unchecked option");
	}

	@Then("The current selected value in '$ControlName' dropdown is '$expectedValue'")
	public void verifySelectedValueInDDL(String contrloName, String expectedValue){
		String currentlySelectedValueOfDDL = reflectionSteps.getCurrentlySelectedValueOfDDL(contrloName);
		Assert.assertThat(currentlySelectedValueOfDDL, Matchers.equalToIgnoringCase(expectedValue));
	}

    @Then("The '$pageName' page is loaded")
    @Given("The '$pageName' page is loaded")
    public void thenThePageIsLoaded(String pageName){
        reflectionSteps.verifyOpenedPage(pageName);
    }
}