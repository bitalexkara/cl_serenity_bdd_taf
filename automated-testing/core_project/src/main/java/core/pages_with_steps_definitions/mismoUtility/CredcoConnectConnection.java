package core.pages_with_steps_definitions.mismoUtility;

import org.apache.http.HttpEntity;
import org.apache.http.ProtocolVersion;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.CertificateException;

public class CredcoConnectConnection {

    public static final Logger logger = LogManager.getLogger();
    public final String USER_AGENT = "InternetExplorer/11";
    String httpConnectionURL;
    SSLContext sslcontext;

    // use this constructor for non-secure connections
    public CredcoConnectConnection(String url) {
        httpConnectionURL = url;
        sslcontext = null;
    }

    // use this constructor for secure connections
    public CredcoConnectConnection(String url, String pathToJKSStore, String keyStorePassword, String certificatePassword) throws KeyStoreException, KeyManagementException, UnrecoverableKeyException, NoSuchAlgorithmException, CertificateException, IOException {
        httpConnectionURL = url;
        final char[] JKS_PASSWORD = keyStorePassword.toCharArray();
        KeyStore keyStore = KeyStore.getInstance("JKS");
        final InputStream is = new FileInputStream(pathToJKSStore);
        keyStore.load(is, JKS_PASSWORD);
        sslcontext = SSLContexts.custom()
                .loadKeyMaterial(keyStore, certificatePassword.toCharArray())
                .build();
    }

    public static String getFilePathFromMismoRequestsEnvironmentFolder(String fileName) {
        String filePath = System.getProperty("mismo.requests") + fileName;

        return filePath;
    }

    public static String readFileToString(String path) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, StandardCharsets.UTF_8);
    }

    public int getHttpConnectionStatus() {
        int responseCode = 503;

        HttpsURLConnection connection = null;

        try {
            URL siteURL = new URL(httpConnectionURL);
            connection = (HttpsURLConnection) siteURL.openConnection();

            if (sslcontext != null) {
                connection.setSSLSocketFactory(sslcontext.getSocketFactory());
            }

            connection.setRequestMethod("GET");
            connection.connect();

            responseCode = connection.getResponseCode();
            logger.info("Connection status of " + httpConnectionURL + "\t\tStatus: " + responseCode);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        if (connection != null) {
            connection.disconnect();
        }

        return responseCode;
    }

    public String postAndGetResponse(String request, String requestName)throws Exception {
        String responseContent = "";
        CloseableHttpClient httpClient = null;
        CloseableHttpResponse httpResponse = null;

        try {
            if (sslcontext != null) {
                SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                        sslcontext,
                        new String[]{"TLSv1.2"},
                        null,
                        SSLConnectionSocketFactory.getDefaultHostnameVerifier());
                httpClient = HttpClients.custom()
                        .setSSLSocketFactory(sslsf)
                        .build();
            } else {
                httpClient = HttpClients.createDefault();
            }

            HttpPost httpPost = new HttpPost(httpConnectionURL);
            StringEntity input = new StringEntity(request);
            httpPost.setProtocolVersion(new ProtocolVersion("HTTP", 1, 1));
            httpPost.setEntity(input);

            httpResponse = httpClient.execute(httpPost);
            StatusLine statusLine = httpResponse.getStatusLine();
            logger.info("Response Code for (" + requestName + "): --> " + httpPost.getRequestLine() + " --> " + statusLine);

            HttpEntity responseEntity = httpResponse.getEntity();
            responseContent = getStringFromInputStream(responseEntity.getContent());
            if (responseEntity != null) {
                logger.info("Response content length: " + responseEntity.getContentLength());
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        if (httpResponse != null) {
            httpResponse.close();
        }

        if (httpClient != null) {
            httpClient.close();
        }

        return responseContent;
    }

    public void getRequestAndSaveResponseToFile(String url, String responseFilePath) {
        try {
            URL obj = new URL(url);

            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = con.getResponseCode();
            logger.info("\nSending 'GET' request to URL: " + url);
            logger.info("Response Code: " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            InputStream resp = new ByteArrayInputStream(response.toString().getBytes());

            saveResponseToFile(resp, responseFilePath);
        } catch (MalformedURLException mue) {
            throw new RuntimeException("Error while parsing URL.", mue);
        } catch (IOException ioe) {
            throw new RuntimeException("An IOException occurred while sending GET request.", ioe);
        }
    }

    public String getAttributeValue(String responseFilePath, String xmlTagName, String attributeName) throws Exception {
        Document xmlDocument = null;
        InputStream is = new FileInputStream(responseFilePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        xmlDocument = dBuilder.parse(is);
        xmlDocument.getDocumentElement().normalize();

        NodeList nList = xmlDocument.getElementsByTagName(xmlTagName);

        if (nList.getLength() == 0) {
            throw new Exception("envKey with name: " + xmlTagName + " not found in the test resource file "
                    + System.getProperty("test.resources"));
        }

        NamedNodeMap attributes = nList.item(0).getAttributes();
        String creditReportID = attributes.getNamedItem(attributeName).getNodeValue();

        return creditReportID;
    }

    public static void createDirIfNotExist(String path) {
        File theDir = new File(path);
        if (!theDir.exists()) {
            System.out.println("creating directory: " + theDir.getName());
            theDir.mkdirs();
            System.out.println("DIR created: " + path);
        }

    }


    private String getStringFromInputStream(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        try {
            String line;

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

    private void saveResponseToFile(InputStream responseInputStream, String responseFilePath) throws IOException {
        FileOutputStream fos = new FileOutputStream(new File(responseFilePath));
        int inByte;
        while ((inByte = responseInputStream.read()) != -1) {
            fos.write(inByte);
        }
        responseInputStream.close();
        fos.flush();
        fos.close();
    }
}