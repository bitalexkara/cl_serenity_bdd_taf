package core.pages_with_steps_definitions.mismoUtility;

import core.pages_with_steps_definitions.BaseSteps;
import core.utils.EnvironmentUtils;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static core.pages_with_steps_definitions.mismoUtility.CredcoConnectConnection.getFilePathFromMismoRequestsEnvironmentFolder;

public class CredcoConnectSteps extends BaseSteps {

    private static final String PROJECT_SYSTEM_PATH = System.getProperty("user.dir");
    private String REPORT_ID_IDENTIFIER = "(CreditReportIdentifier=\")(\\d{11}0{4})";
    private String PFM_REPORT_IDENTIFIER = "(CreditReportIdentifier=\")(PFM0{4}\\d{8})";
    private String STATUS_CONDITION = "(Condition=\")(\\w*)(\")";
    private String UNASSIGNED_STATUS_CONDITION = "(Condition=\")(\\w* )(\")";

    private static CredcoConnectConnection connection = null;

    @Step
    public int setUrlAndOpenCredcoConnectConnection(String url) throws Exception {
		boolean useSecureProtocol = Boolean.parseBoolean(EnvironmentUtils.getValueByXMLKeyFromTestResources("useSecureProtocol"));

		String test = PROJECT_SYSTEM_PATH + EnvironmentUtils.getValueByXMLKeyFromTestResources("jksCertificatePath");

		if (useSecureProtocol) {
			connection = new CredcoConnectConnection(url, PROJECT_SYSTEM_PATH + EnvironmentUtils.getValueByXMLKeyFromTestResources("jksCertificatePath"),
					   EnvironmentUtils.getValueByXMLKeyFromTestResources("certificateStorePassword"),
					   EnvironmentUtils.getValueByXMLKeyFromTestResources("certificatePassword"));
		}
		else {
			connection = new CredcoConnectConnection(url);
		}

		int responseCode = connection.getHttpConnectionStatus();
        return responseCode;
    }

    @Step
    public String sendUpgradeRequest(String requestFile, String creditReportIdentifier) throws Exception {
        String request = "";
        String envRequestFileName = EnvironmentUtils.getEnvironmentDependentValue(requestFile);
        String envRequestFilePath = getFilePathFromMismoRequestsEnvironmentFolder(envRequestFileName);

        if (requestFile.contains("StatusQuery")) {
            request = CredcoConnectConnection.readFileToString(envRequestFilePath).replace("replaceMe1", creditReportIdentifier);
        } else {
            String creditReportTransactionIdentifier = creditReportIdentifier.replace("0000", "");
            request = CredcoConnectConnection.readFileToString(envRequestFilePath).replace("replaceMe1", creditReportIdentifier).replace("replaceMe2", creditReportTransactionIdentifier);
        }

        String responseBody = connection.postAndGetResponse(request, envRequestFileName);
        return responseBody;
    }

    @Step
    public String retrieveTid(String type, String currentResponse) {
        Pattern pattern;
        pattern = type.contains("IM_") ? Pattern.compile(REPORT_ID_IDENTIFIER) : Pattern.compile(PFM_REPORT_IDENTIFIER);

        Matcher matcher = pattern.matcher(currentResponse);
        String result = null;
        while (matcher.find()) {
            result = matcher.group(2);
            break;
        }

        return result;
    }

    @Step
    public String sendRequest(String requestFileName) throws Exception {
        String envRequestFileName = EnvironmentUtils.getEnvironmentDependentValue(requestFileName);
        envRequestFileName = envRequestFileName.contains(".xml") ? envRequestFileName: envRequestFileName+".xml";
        String envRequestFilePath = getFilePathFromMismoRequestsEnvironmentFolder(envRequestFileName);

        String responseBody = connection.postAndGetResponse(
                CredcoConnectConnection.readFileToString(envRequestFilePath), envRequestFileName);
        return responseBody;
    }

    @Step
    public void sendGetRequestAndSaveResponse(String urlKey, String responsesFolder, String responseFileKey) throws Exception {
        String responseFileName = createResponseFileName(responseFileKey);
        String envResponsesFolder = EnvironmentUtils.getEnvironmentDependentValue(responsesFolder);
        CredcoConnectConnection.createDirIfNotExist(envResponsesFolder);
        String envResponseFilePath = envResponsesFolder + '/' + responseFileName;

        String url = Serenity.sessionVariableCalled(urlKey);
        Serenity.setSessionVariable(responseFileKey).to(envResponseFilePath);
        connection.getRequestAndSaveResponseToFile(url, envResponseFilePath);
    }

    @Step
    public String getStatusCondition(String statusContidion, String currentResponse) {
        Pattern pattern;
        if (statusContidion.contains("Unassigned")) {
            pattern = Pattern.compile(UNASSIGNED_STATUS_CONDITION);
        } else {
            pattern = Pattern.compile(STATUS_CONDITION);
        }

        Matcher matcher = pattern.matcher(currentResponse);
        String result = null;
        while (matcher.find()) {
            result = matcher.group(2);
            break;
        }
        return result;
    }

    private String createResponseFileName(String requestFileName) {
        // Set Date and Time into result file name
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String tagResponseName = "Response_" + dateFormat.format(date);
        String responseFileName = requestFileName.replace("Request", tagResponseName);
        return responseFileName;
    }

}