package core.pages_with_steps_definitions.mismoUtility;

import core.entities.ExampleMap;
import core.utils.EnvironmentUtils;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hamcrest.Matchers;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.junit.Assert;

import java.util.*;

import static core.pages_with_steps_definitions.mismoUtility.CredcoConnectDefinitions.RequestParameters.builderParameters;
import static org.hamcrest.CoreMatchers.not;

public class CredcoConnectDefinitions {
    private static final Logger logger = LogManager.getLogger();
	private static final String SHORT_LENGTH_RESPONSE_ERROR_MESSAGE = "The content length of the response is too short: ";
	private static final String REPORT_ID_MESSAGE = "Report ID retrieved from mismo response:-> ";
	private static final String STATUS_CONDITION_MESSAGE = "Status condition from status query response:-> ";
	private static CredcoConnectConnection connection = null;
	private String currentResponse = null;
	private static Map<String, String> applicantData = new HashMap<String, String>();
    private static List<RequestParameters> requestParametersList;

    @Steps
	CredcoConnectSteps credcoConnectSteps;

	@Given("The credcoconnect service is accessible by '$credcoConnectURL'")
	public void setUrlAndOpenCredcoConnectConnection(String envUrlKey) throws Exception {
		String url = EnvironmentUtils.getEnvironmentDependentValue(envUrlKey);
		int responseCode = credcoConnectSteps.setUrlAndOpenCredcoConnectConnection(url);

		Assert.assertTrue("CredcoConnect connection fails; url = " + url, responseCode == 200);
	}

	@Given("I collect reportId for '$requestType' using Mismo and save to '$keyReferenceNumber' for further product ordering")
	@When("I collect reportId for '$requestType' using Mismo and save to '$keyReferenceNumber' for further product ordering")
	public void prepareReportID(String requestType, String keyReferenceNumber) throws Exception {
		setUrlAndOpenCredcoConnectConnection("envKey_credcoConnectURL");
		sendRequest(requestType);
		retrieveTID(requestType, keyReferenceNumber);
	}

    @When("I send '$requestFile' request to CredcoConnect")
	public void sendRequest(String requestFileName) throws Exception {
		String responseBody = credcoConnectSteps.sendRequest(requestFileName);
		logger.trace(responseBody);
		Assert.assertTrue(SHORT_LENGTH_RESPONSE_ERROR_MESSAGE + responseBody, responseBody.length() > 500);
		currentResponse = responseBody;
    }

	@When("I send '$requestFile' {upgrade|status query} request for '$creditReportIdentifier' order")
    public void sendUpgradeRequest(String requestFile, String creditReportIdentifier) throws Exception {
		String responseBody = credcoConnectSteps.sendUpgradeRequest(requestFile, creditReportIdentifier);
        logger.trace(responseBody);
        Assert.assertTrue(SHORT_LENGTH_RESPONSE_ERROR_MESSAGE + responseBody, responseBody.length() > 500);
        currentResponse = responseBody;
	}

	@When("I send GET request for '$urlKey' and save the response to '$responsesFolder' and assign the file path to '$responseFileKey' key")
	public void sendGetRequestAndSaveResponse(String urlKey, String responsesFolder, String responseFileKey) throws Exception {
		credcoConnectSteps.sendGetRequestAndSaveResponse(urlKey, responsesFolder, responseFileKey);
	}

	@Then("Applicant information from response '$responseFileKey' entered earlier in credco.com is present:$infoTable")
	public void checkAppletIndentificationData(String responseFileKey, ExamplesTable infoTable) throws Exception {
		Map<String, String> exampleTableRow = infoTable.getRow(0);
		Map<String, String> envExampleTableRow = EnvironmentUtils.getEnvironmentDependentExampleTableRow(exampleTableRow);
		ExampleMap expectedApplicantData = new ExampleMap(envExampleTableRow);
		ExampleMap actualApplicantData = initiateMapWithActualApplicantData(responseFileKey);

		String comparisonErrors = actualApplicantData.getDiffLogs(expectedApplicantData, null);
		Assert.assertTrue(comparisonErrors, comparisonErrors.isEmpty());
	}

	@Then("I retrieve '$type' transaction ID and save to '$creditReportIdentifier' session variable")
    public void retrieveTID(String type, String creditReportIdentifier){
		String reportID = credcoConnectSteps.retrieveTid(type, currentResponse);
		Serenity.setSessionVariable(creditReportIdentifier).to(reportID);
		Assert.assertThat("Response is empty for '" + type + "' product type", reportID, not(Matchers.isEmptyOrNullString()));
		logger.debug(REPORT_ID_MESSAGE + reportID);
		currentResponse = null;
	}

	@Then("Status condition is '$expectedStatusContidion'")
	public void checkStatusContidion(String expectedStatusContidion) {
		String actualStatusContidion = credcoConnectSteps.getStatusCondition(expectedStatusContidion, currentResponse);

		Assert.assertTrue("Expected Status Condition: " + expectedStatusContidion + ". Actual: " + actualStatusContidion, actualStatusContidion.equalsIgnoreCase(expectedStatusContidion));
		logger.debug(STATUS_CONDITION_MESSAGE + actualStatusContidion);
		currentResponse = null;
	}

	@When("I wait untill mismo requests will be processed")
	public void waitForProcessing() throws InterruptedException {
    	Thread.sleep(15000);
	}

	private static ExampleMap initiateMapWithActualApplicantData(String responseFileKey) {
		RequestParameters.setResponseFileKey(responseFileKey);
		RequestParameters.setConnection(connection);
		requestParametersList = Arrays.asList(builderParameters("Last Name","BORROWER", "_LastName"),
				builderParameters("First Name", "BORROWER", "_FirstName"), builderParameters("SSN", "BORROWER", "_SSN"),
				builderParameters("Loan Number", "CREDIT_REQUEST", "LenderCaseIdentifier"), builderParameters("Num", "PARSED_STREET_ADDRESS", "_HouseNumber"),
				builderParameters("Street", "PARSED_STREET_ADDRESS", "_StreetName"), builderParameters("City", "_RESIDENCE", "_City"),
				builderParameters("State", "_RESIDENCE", "_State"), builderParameters("Zip", "_RESIDENCE", "_PostalCode"),
						builderParameters("Date Of Birth", "BORROWER", "_BirthDate"));
		requestParametersList.forEach(el -> applicantData.put(el.field, el.value));

		return new ExampleMap(applicantData);
	}

	public static class RequestParameters {
		private final String field;
		private final String tag;
		private final String attribute;
		private String value;

		public static String responseFileKey;

		public static CredcoConnectConnection connection;

		private RequestParameters(String field, String tag, String attribute) {
			this.field = field;
			this.tag = tag;
			this.attribute = attribute;
			value = getAttributeValueFromResponseFile();
		}

		public static void setResponseFileKey(String responseFileKey) {
			RequestParameters.responseFileKey = responseFileKey;
		}

		public static void setConnection(CredcoConnectConnection connection) {
			RequestParameters.connection = connection;
		}

		public static RequestParameters builderParameters (String field, String tag, String attribute) {
			return new RequestParameters(field, tag, attribute);
		}

		public String getAttributeValueFromResponseFile() {
			try {
				String responseFilePath = null;
				if (Serenity.hasASessionVariableCalled(responseFileKey)) {
					responseFilePath = Serenity.sessionVariableCalled(responseFileKey);
				}
				String xmlAttribute = attribute;
				String xmlTag = tag;

				String attributeValue = connection.getAttributeValue(responseFilePath, xmlTag, xmlAttribute);
				logger.debug("Response File = " + responseFilePath + " ;; " + xmlAttribute + "=" + attributeValue);
				return attributeValue;
			} catch (Exception e) {
				throw new RuntimeException("Expected occurred " + e.getMessage());
			}
		}
	}
}
