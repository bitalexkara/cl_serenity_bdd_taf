package core.pages_with_steps_definitions;

import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.BeforeStory;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

/**
 * Created by okara on 1/30/2017.
 */
public class BaseDefinitions {
    @Steps
    BaseSteps baseSteps;
    
    @BeforeStory
	public void setUp() {
		baseSteps.openBrowserInMaximiseMode();
	}
    
    @Then("The window is closed and '$pageTitle' window is switched")
    @When("I close window and switch to '$pageTitle' window")
    public void windowIsClosed(String pageTitle) throws Exception {
        baseSteps.closeWindowAndSwitchToWindowByTitle(pageTitle);
    }

    @Then("The '$expectedAlertValue' text is present in browser's alert")
    public void TheValueTextIsPresentInBrowsersAlert(String expectedAlertValue) {
        String actualAlertValue = baseSteps.getAlertText();

        Assert.assertEquals(expectedAlertValue, actualAlertValue);
    }
}