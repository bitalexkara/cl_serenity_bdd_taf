package core.pages_with_steps_definitions;

import core.utils.driver.DriverUtils;
import core.utils.element.IWebElementFacade;
import core.utils.reflection.Page;
import core.utils.reflection.annotations.*;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.SECONDS;

public class ReflectionSteps extends BaseSteps {
	private static final Logger logger = LogManager.getLogger();
	private static int DEFAULT_ELEMENT_LOOKUP_TIMEOUT = 10;

	public ReflectionSteps(Pages pages) {
		super(pages);
	}

	@Step
	public BasePage openPage(String pageName) {
		BasePage page = Page.get(pageName, getDriver());
		page.open();	
		Serenity.setSessionVariable(currentPageKey).to(page);
		
		return page;
	}

	@Step
	public void clickOnTheButtonOrLink(String buttonLinkName){
		clickOnControl(buttonLinkName, ButtonLink.class);
	}

	@Step
	public void selectRadioButton(String radioButtonName){
		clickOnControl(radioButtonName, RadioButton.class);
	}

	@Step
	public void selectCheckBox(String checkBoxName, boolean isToSelect){
		BasePage page = Serenity.sessionVariableCalled(currentPageKey);
		String selector = Page.getSelector(page, checkBoxName);
		DriverUtils.waitForPageLoad(getDriver());
		IWebElementFacade elem = page.element(selector, DEFAULT_ELEMENT_LOOKUP_TIMEOUT, SECONDS);
        if ((!elem.isSelected() && isToSelect) || (elem.isSelected() && !isToSelect)) {
            page.retryingFindClick(selector);
        }
	}

	@Step
	public boolean isCheckBoxSelected(String checkBoxName){
		BasePage page = Serenity.sessionVariableCalled(currentPageKey);
		String selector = Page.getSelector(page, checkBoxName);
		DriverUtils.waitForPageLoad(getDriver());
		IWebElementFacade elem = page.element(selector, DEFAULT_ELEMENT_LOOKUP_TIMEOUT, SECONDS);
		return elem.isSelected();
	}

	private void clickOnControl(String controlName, Class controlType) {
        BasePage page = Serenity.sessionVariableCalled(currentPageKey);
        String selector = Page.getSelector(page, controlName, controlType);
        DriverUtils.waitForPageLoad(getDriver());
		page.retryingFindClick(selector);
    }

    @Step
    public void verifyOpenedPage(String pageName){
        BasePage page = Page.get(pageName, getDriver());
        page.verify();
        Serenity.setSessionVariable(currentPageKey).to(page);
    }

    private void typeValueToTheField_NotAStep(String value, String fieldName){
        BasePage page = Serenity.sessionVariableCalled(currentPageKey);
        String selector = Page.getSelector(page, fieldName);
        DriverUtils.waitForPageLoad(getDriver());
        page.retryingFindType(selector, value);
    }

    @Step("I enter credentials value to the field '{1}'")
    public void typeCredentialsValueToTheField(String value, String fieldName){
        typeValueToTheField_NotAStep(value, fieldName);
    }

	@Step
    public void typeValueToTheField(String value, String fieldName){
        typeValueToTheField_NotAStep(value, fieldName);
    }

    @Step
	public void setValueToTheField_JSHack(String value, String fieldName){
		BasePage page = Serenity.sessionVariableCalled(currentPageKey);
		String selector = Page.getSelector(page, fieldName);
		WebElementFacade el = page.$(selector);
		String id = el.getAttribute("id");
		String script = String.format("document.getElementById('%s').value='%s'", id, value);

		logger.info("Script for setting value to the field using JS prepared -> " + script);
		JavascriptExecutor jsExec = (JavascriptExecutor) page.getDriver();
		jsExec.executeScript(script);
	}

    @Step
    public void clearField(String fieldName) throws Exception {
        BasePage page = Serenity.sessionVariableCalled(currentPageKey);
        String selector = Page.getSelector(page, fieldName);
        DriverUtils.waitForPageLoad(getDriver());
        page.element(selector, DEFAULT_ELEMENT_LOOKUP_TIMEOUT, SECONDS).clear();
    }

	@Step
	public void selectValueFromDropDown(String value, String dropdownName){
		BasePage page = Serenity.sessionVariableCalled(currentPageKey);
		String selector = Page.getSelector(page, dropdownName);
        IWebElementFacade element = page.element(selector, DEFAULT_ELEMENT_LOOKUP_TIMEOUT, SECONDS);
        element
                .waitUntilClickable()
                .selectByVisibleText(value);
	}

	@Step
	public void clickOnTheButtonOrLinkAndVerifyPage(String buttonLinkName, String pageNameAfterClick){
		clickOnTheButtonOrLink(buttonLinkName);
		verifyOpenedPage(pageNameAfterClick);
	}
	
	@Step
	public void clickOnTheButtonOrLinkWithNewWindowOpened(String buttonLinkName, String windowName){
	    clickAndSwitchToNewWindow(buttonLinkName, 20, SECONDS);
        verifyOpenedPage(windowName); //ideally we should have separate method for window
    }

    private void clickAndSwitchToNewWindow(String buttonLinkName, long timeout, TimeUnit unit){
        Set<String> windowsHandlesBeforeAction = getDriver().getWindowHandles();
        clickOnTheButtonOrLink(buttonLinkName);
        waitForWindowsCountChange(windowsHandlesBeforeAction.size(), timeout, unit);
        String newWindow = getNewOpenedWindowHandle(windowsHandlesBeforeAction);
        getDriver().switchTo().window(newWindow);
    }

    @Deprecated
    @Step
	public boolean isControlExists(String controlName, Class controlType){
		return isControlExists(controlName);
	}

	@Step
	public boolean isControlExists(String controlName){
		BasePage page = Serenity.sessionVariableCalled(currentPageKey);
		String selector = Page.getSelector(page, controlName);
		int elements = page.findAll(selector).size();

		return elements > 0;
	}

	@Deprecated
	@Step
	public boolean isControlEnabled(String controlName, Class controlType){
		return isControlEnabled(controlName);
	}

	@Step
	public boolean isControlEnabled(String controlName){
		BasePage page = Serenity.sessionVariableCalled(currentPageKey);
		String selector = Page.getSelector(page, controlName);
		DriverUtils.waitForPageLoad(getDriver());
		IWebElementFacade webElem = page.element(selector, DEFAULT_ELEMENT_LOOKUP_TIMEOUT, SECONDS);

		return webElem.isEnabled();
	}

	@Deprecated
	@Step
	public String getControlText(String buttonLinkName, Class controlType){
		return getControlText(buttonLinkName);
	}

	/**
	 * Method is able to retrieve value attribute from input tag(<input type='text' value='some text'><input/>),
	 * as well as text between tags (ex. <p>some text<p/>)
	 * @param buttonLinkName
	 * @return
	 * @throws IllegalAccessException
	 */
	@Step
	public String getControlText(String buttonLinkName){
		BasePage page = Serenity.sessionVariableCalled(currentPageKey);
		String selector = Page.getSelector(page, buttonLinkName);
		IWebElementFacade element = page.element(selector, DEFAULT_ELEMENT_LOOKUP_TIMEOUT, SECONDS);
		String controlText = element
				.waitUntilClickable()
				.getTextValue();

		theTextValueHasBeenFound(controlText);

		return controlText;
	}

    @Step
    public void openAndVerifyPage(String pageName){
        String newBaseUrl = getUrlValueFromParams();
        setBaseUrlValue(newBaseUrl);

        BasePage loginPage = openPage(pageName);
        loginPage.verify();
    }

    /**
     * Try to use standart steps first to handle operations on webElement,
     * if nothing helps web element could be retrieved directly.
     * @param elementName
     * @return
     */
    @Deprecated
    @Step
	public IWebElementFacade getWebElementFromCurPageByName(String elementName){
		BasePage page = Serenity.sessionVariableCalled(currentPageKey);
		String selector = null;
		selector = Page.getSelector(page, elementName);
		IWebElementFacade element = page.element(selector, DEFAULT_ELEMENT_LOOKUP_TIMEOUT, SECONDS);
		return element;
	}

	@Step
	public String getCurrentlySelectedValueOfDDL(String elementName){
		BasePage page = Serenity.sessionVariableCalled(currentPageKey);
		String selector = Page.getSelector(page, elementName);
		IWebElementFacade element = page.element(selector, DEFAULT_ELEMENT_LOOKUP_TIMEOUT, SECONDS);
		return element.getSelectedVisibleTextValue();
	}
}
