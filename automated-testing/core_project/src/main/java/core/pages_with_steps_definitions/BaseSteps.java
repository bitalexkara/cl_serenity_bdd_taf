package core.pages_with_steps_definitions;

import com.google.common.base.Stopwatch;
import com.opera.core.systems.scope.exceptions.WindowNotFoundException;
import core.utils.EnvironmentUtils;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import net.thucydides.core.ThucydidesSystemProperty;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.UnhandledAlertException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class BaseSteps extends ScenarioSteps {
    final public static String currentPageKey = "current_page";

	public BaseSteps() {
		super();
	}

	public BaseSteps(final Pages pages) {
		super(pages);
    }
	
	/**
    * Clicks keyword key 
    */
	public void clickKey(Keys key) {
		Keys.chord(key);
	}

	/*
	Represents text value to report
	No implementation is required
	 */
	@Step
	public void theTextValueHasBeenFound(String textValue) {/*theUrlHasBeenChanged is needed for report purposes*/}

	@Step
    protected void theUrlHasBeenChanged(String textValue) {/*theUrlHasBeenChanged is needed for report purposes*/}

	/**
	 * Maximizes the current window if it is not already maximized
	 */
	public void openBrowserInMaximiseMode() {
		getDriver().manage().window().maximize();
	}

	/**
	 * Sets serenity property webdriver.base.url to serenity.properties
	 */
    public void setBaseUrlValue(String newBaseUrl) {
		pages().getConfiguration().getEnvironmentVariables().setProperty(ThucydidesSystemProperty.WEBDRIVER_BASE_URL.getPropertyName(), newBaseUrl);
	}

    /**
     * Gets url values from test resources
     * @throws Exception
     */
    public String getUrlValueFromParams(){
        String res = "WasUndefined!!!";
        try {
            res = EnvironmentUtils.getValueByXMLKeyFromTestResources("BaseUrl");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return res;
    }

	public void goToUrl(String pageUrl) {
		getDriver().get(pageUrl);
	}

    public int getWindowsCount() {
		
		return getDriver().getWindowHandles().size();
	}

	@Step
    public String getAlertText() {
        
		return getDriver().switchTo().alert().getText();
    }

    @Step
    public void acceptAlert() {
        getDriver().switchTo().alert().accept();
    }
    
	@Step
    private boolean isAlertPresent() {
        try {
            getDriver().switchTo().alert();
            
            return true;
        } catch (NoAlertPresentException Ex) {
            
        	return false;
        }
    }

    @Step
    public void closeWindowAndSwitchToWindowByTitle(String expectedPageTitle) throws NoSuchWindowException {
        String currentHandleBeforeAction = getDriver().getWindowHandle();
        Set<String> windowsHandlesBeforeAction = getDriver().getWindowHandles();
        getDriver().close();
        waitForWindowsCountChange(windowsHandlesBeforeAction.size(), 10, SECONDS);
        Set<String> windowsHandlesAfterAction = getDriver().getWindowHandles();
        for(String handle: windowsHandlesAfterAction) {
            if(!handle.equals(currentHandleBeforeAction)) {
                getDriver().switchTo().window(handle);
                try {
                    if (StringUtils.containsIgnoreCase(getDriver().getTitle(), expectedPageTitle)) {
                        return;
                    }
                }
                catch (NoSuchWindowException ignored){}
            }
        }

        throw new NoSuchWindowException(String.format("Window is not found with '%s' page title.", expectedPageTitle));
    }

    protected void waitForWindowsCountChange(int windowsCountBeforeAction, long timeout, TimeUnit unit) {
        timeout = TimeUnit.MILLISECONDS.convert(timeout, unit);
        Stopwatch stopwatch = Stopwatch.createStarted();

        while(stopwatch.elapsed(MILLISECONDS) < timeout ) {
            if (windowsCountBeforeAction != getDriver().getWindowHandles().size()) {
                return;
            }
        }
    }

    protected void waitForAlertIsPresent(long timeout, TimeUnit unit) {
        timeout = TimeUnit.MILLISECONDS.convert(timeout, unit);
        Stopwatch stopwatch = Stopwatch.createStarted();

        while(stopwatch.elapsed(MILLISECONDS) < timeout ) {
            try {
                getDriver().switchTo().alert();
                return;
            } catch (NoAlertPresentException|UnhandledAlertException ignored) {}
        }
    }

    protected String getNewOpenedWindowHandle(Set<String> windowsHandlesBeforeAction) throws WindowNotFoundException {
        Set<String> windowsHandlesAfterAction = getDriver().getWindowHandles();
        if (windowsHandlesAfterAction.size() > windowsHandlesBeforeAction.size()) {
            windowsHandlesAfterAction.removeAll(windowsHandlesBeforeAction);
        } else throw new WindowNotFoundException(String.format("Count of opened windows before action didn't increase. Opening new window is expected. \nCount before action: '%d'; Count after action: '%d'.", windowsHandlesBeforeAction.size(), windowsHandlesAfterAction.size()));
        int countOfNewOpenedWindows = windowsHandlesAfterAction.size();
        if (countOfNewOpenedWindows != 1) {
            throw new WindowNotFoundException("Count of new opened windows in not equal to one. Actual count: " + countOfNewOpenedWindows);
        }

        return (String) windowsHandlesAfterAction.toArray()[0];
    }
}