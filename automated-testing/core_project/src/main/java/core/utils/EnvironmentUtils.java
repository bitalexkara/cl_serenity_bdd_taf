package core.utils;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class EnvironmentUtils {

    final static String envKeyPrefix = "envKey_";
    protected static Document xmlDocument = null;
    private static final Logger logger = LogManager.getLogger();

    /**
     * Call this function to replace envKey_ value defined in the scenario step with value from testResouces file.
     * If the input value does not contain "envKey_" prefix, it is returned as is, without changes.
     *
     * @param stepValue - the value from the scenario step that may contain "envKey_" prefix
     * @return value from testResouces file
     * @throws Exception
     */
    public static String getEnvironmentDependentValue(String stepValue) throws Exception {
        if (!stepValue.contains(envKeyPrefix)) {

            return stepValue;
        }
        String xmlKey = stepValue.replace(envKeyPrefix, "");

        return getValueByXMLKeyFromTestResources(xmlKey);
    }

    /**
     * Call this function to replace envKey_ value(s) defined in the table row in scenario step with value(s) from testResouces file.
     * If the input value does not contain "envKey_" prefix, it is returned as is, without changes.
     *
     * @param row from ExampleMap defined in story file
     * @return new row where all values prefixed with "envKey_" are replaced with values from testResouces file
     * @throws Exception
     */
    public static Map<String, String> getEnvironmentDependentExampleTableRow(Map<String, String> row) throws Exception {
        Map<String, String> updatedExampleMapRow = new HashMap<String, String>();

        for (Map.Entry<String, String> exampleMapEntry : row.entrySet()) {
            String columnName = exampleMapEntry.getKey();
            String columnValue = exampleMapEntry.getValue();
            String updatedColumnValue = getEnvironmentDependentValue(columnValue);
            updatedExampleMapRow.put(columnName, updatedColumnValue);
        }

        return updatedExampleMapRow;
    }

    /**
     * Looks for the unique xml tag in the file defined by test.resources property and return its text value
     *
     * @param xmlKey- xml tag name
     * @return value from testResouces file
     * @throws Exception if the input xml key is not found or found more than once, exception is produced
     */
    public static String getValueByXMLKeyFromTestResources(String xmlKey) throws Exception {
        initXMLDocument(System.getProperty("test.resources"));
        NodeList nList = xmlDocument.getElementsByTagName(xmlKey);

        isSingleNodeReturned(nList, xmlKey);

        String value = nList.item(0).getTextContent();
        return value;
    }

    public static void setValueByXMLKeyToTestResources(String xmlKey, String value) throws Exception {
        initXMLDocument(System.getProperty("test.resources"));
        NodeList nList = xmlDocument.getElementsByTagName(xmlKey);

        isSingleNodeReturned(nList, xmlKey);

        if (isCurrentXmlKeyValueOutdated(xmlKey, value)) {
            nList.item(0).setTextContent(value);
            updateXmlFile();
            System.out.printf("Node %s in test resource is updated with value: %s ", xmlKey, value);
        }
    }

    private static void isSingleNodeReturned(NodeList nList, String xmlKey) throws Exception {
        if (nList.getLength() == 0) {
            throw new Exception("envKey with name: " + xmlKey + " not found in the test resource file "
                    + System.getProperty("test.resources"));
        } else if (nList.getLength() > 1) {
            throw new Exception("Several envKey with name: " + xmlKey + " are found in the test resource file " + System.getProperty("test.resources")
                    + " Xml key name must be unique.");
        }
    }

    private static boolean isCurrentXmlKeyValueOutdated(String xmlKey, String value) throws ParserConfigurationException, SAXException, IOException {
        initXMLDocument(System.getProperty("test.resources"));
        NodeList nList = xmlDocument.getElementsByTagName(xmlKey);
        return !nList.item(0).getTextContent().equals(value);
    }

    protected static void initXMLDocument(String pathToFile) throws SAXException, IOException, ParserConfigurationException {
        InputStream is;
        DocumentBuilderFactory dbFactory;
        DocumentBuilder dBuilder;
        if (xmlDocument == null) {
            dbFactory = DocumentBuilderFactory.newInstance();
            dBuilder = dbFactory.newDocumentBuilder();
            try {
                ClassLoader classloader = Thread.currentThread().getContextClassLoader();
                is = classloader.getResourceAsStream(pathToFile);
                xmlDocument = dBuilder.parse(is);
                xmlDocument.getDocumentElement().normalize();
            }catch (IllegalArgumentException ia){
                logger.warn("Test resource was not succesfully initialized by clasloader. Will try to open as file using absolute path.");
                is = new BufferedInputStream(new FileInputStream(new File(pathToFile)));
                xmlDocument = dBuilder.parse(is);
                xmlDocument.getDocumentElement().normalize();
            }
        }
    }

    private static void updateXmlFile() throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory
                .newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(xmlDocument);
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        String path = classloader.getResource(System.getProperty("test.resources")).getPath();
        StreamResult result = new StreamResult(new File(path));
        transformer.transform(source, result);
    }

    public static String getFileFullPath(String packagePathToFile)
            throws SAXException, IOException, ParserConfigurationException {

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        String[] pdfDocument = classloader.getResource(packagePathToFile).toString().split("file:/");

        return pdfDocument[1].replace("/", "\\");
    }

    public static Document getXmlDocument() {
        return xmlDocument;
    }
}
