package core.utils.driver;

import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import core.pages_with_steps_definitions.BasePage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Timeouts;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import net.thucydides.core.webdriver.WebDriverFacade;

public class DriverUtils {
	private static final Logger logger = LogManager.getLogger();
	/**
	 * Use this function is the page is not loaded in usual way. For example, in IE some pages_with_steps_definitions do not
	 * always report they have finished their loading and it causes WebDriver to wait them endlessly.
	 * This functions forces several attempts for page load
	 * 
	 * @param page  - the page object you want to reload
	 * @param driver - the driver
	 * @param timeLimitForAttempt - how much to wait for 1 page load, in seconds
	 * @param maxNumberOfAttempts - how many attempts to perform
	 */
	//TODO: revise implementation to pass only time, not attempts
	public static void openPageWithAttempts(BasePage page, WebDriver driver, int timeLimitForAttempt, int maxNumberOfAttempts) {
		
		boolean load_is_successfull = false;
		int number_of_attempts = 0;

		Timeouts timeouts = driver.manage().timeouts();
		
		timeouts.pageLoadTimeout(timeLimitForAttempt, TimeUnit.SECONDS);
		timeouts.setScriptTimeout(timeLimitForAttempt, TimeUnit.SECONDS);
        
		while (!load_is_successfull && number_of_attempts <= maxNumberOfAttempts) {
			
			try {
				page.open();
				load_is_successfull = true;
			}
			catch (TimeoutException ex) {
				 logger.warn("Expected TimeoutException exception has happened when waiting for loading page");
			}
			logger.info("Page re-open attempt #: " + number_of_attempts);
			number_of_attempts++;
		}
		
		// return previous state
		driver.manage().timeouts().pageLoadTimeout(600, TimeUnit.SECONDS);
		driver.manage().timeouts().setScriptTimeout(600, TimeUnit.SECONDS);
	}
	
	public static void restartBrowser(WebDriverFacade driver) {
		driver.quit();
		driver.getProxiedDriver(); //this will start new instance of the browser
	}
	
	public static void waitForPageLoad(WebDriver driver) {
		waitForPageLoad(driver, 60L);
	}

	public static void waitForPageLoad(WebDriver driver, long timeOutInSeconds){
        ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {

                return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
            }
        };

        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until(pageLoadCondition);
    }
	
	public static boolean switchToWindowByTitle(WebDriver driver, String title){
	    Set<String> availableWindows = driver.getWindowHandles(); 
	    if (!availableWindows.isEmpty()) { 
	         for (String windowId : availableWindows) {
	              String switchedWindowTitle=driver.switchTo().window(windowId).getTitle();
	              if (switchedWindowTitle.contains(title)) {
	            	    JavascriptExecutor js = (JavascriptExecutor)driver;
	            	    js.executeScript( "window.focus();" );
	            	    
	            	    return true; 
	              } 
	          } 
	     } 
	    
	     return false;
	}
	
	public static void closeAllOpenedWindowsExceptFirstOne(WebDriver driver) {

		int i;
		ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());

		// Close all opened windows except first one
		if (windows.size() != 1) {
			if (windows.size() == 2) {
				driver.switchTo().window(windows.get(windows.size() - 1));
				driver.close();
				driver.switchTo().window(windows.get(windows.size() - 2));
			} else if (windows.size() == 3) {
				driver.switchTo().window(windows.get(windows.size() - 1));
				driver.close();
				driver.switchTo().window(windows.get(windows.size() - 2));
				driver.close();
				driver.switchTo().window(windows.get(windows.size() - 3));
			} else if (windows.size() > 3) {
				for (i = 1; i <= windows.size(); i++) {
					driver.switchTo().window(windows.get(windows.size() - i));
					driver.close();
				}

				driver.switchTo().window(windows.get(windows.size() - windows.size()));
			}
		}
	}

	public static boolean isAlertPresent(WebDriver driver) {
		boolean foundAlert = false;
		WebDriverWait wait = new WebDriverWait(driver, 10);
		try {
			wait.until(ExpectedConditions.alertIsPresent());
			foundAlert = true;
		} catch (TimeoutException eTO) {
			foundAlert = false;
		}
		return foundAlert;
	}
}