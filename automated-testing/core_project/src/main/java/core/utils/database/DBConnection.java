package core.utils.database;

import core.utils.EnvironmentUtils;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	
	private static volatile Connection connection ;
	private DBConnection() {}
	
	/**
	 * Creates the connection for SQL database basing on the connection string, user and password
	 * @return connection object
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	private static Connection DatabaseConnection() throws Exception {
		Connection connection = null;
		String dbAddress = EnvironmentUtils.getValueByXMLKeyFromTestResources("dbAddress");
		String dbUser = EnvironmentUtils.getValueByXMLKeyFromTestResources("dbUser");
		String dbPassword = EnvironmentUtils.getValueByXMLKeyFromTestResources("dbPassword");
		try {
			String dbDriverName = System.getProperty("db.driver.name");
			Class.forName(dbDriverName);
			
			connection = DriverManager.getConnection(dbAddress, dbUser, dbPassword);
			
			System.out.println("db connection opened...........");
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
			throw sqlEx;
		}

		return connection;
	}

	/**
	 * Use this method to get Connection object. It returns existing Connection 
	 * or creates new one if the Connection does not exist. So only 1 connection is possible.
	 * @return connection object
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static Connection getDBConnection() throws Exception {
		if(connection == null || connection.isClosed()){
			connection = DatabaseConnection();
		}

		return connection;
	}

	/**
	 * Invoke this method each time when you don't need the connection any more 
	 * to free DB resources for other usage
	 */
	public static void closeConnection() throws SQLException {
		try {
			connection.close();
			connection = null;

		} catch (SQLException e) {
			try {
				connection.close();
				connection = null;
			} catch (SQLException e1) {
				e1.printStackTrace();
				throw e1;
			}
		}
	}
}