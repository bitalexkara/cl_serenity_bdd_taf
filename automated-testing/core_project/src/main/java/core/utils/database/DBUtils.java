package core.utils.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public class DBUtils {

	private DBUtils() {	}
	
	/**
	 * The functions executes the update query
	 * @param query  - string that contains the "udpate" query
	 * @return number of rows updated with the query
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	
	public static long updateSQL(String query) throws SQLException, ClassNotFoundException, Exception {
		long numberOfRows = 0;
		
		try {
			Connection connection = DBConnection.getDBConnection();
			Statement statement = connection.createStatement();
			
			numberOfRows = statement.executeUpdate(query);
			System.out.println("# of rows updated:: " + numberOfRows );

		} catch (SQLException e) {
			e.printStackTrace();
			DBConnection.closeConnection();
			throw e;
		}
		
		return numberOfRows;
	}
	
	/**
	 * The functions executes the select  query
	 * @param query  - string that contains the "select" query
	 * @return ArrayList of HashMaps with results. Each db row is saved into HashMap (key=column name, value=cellvalue)
	 * and each row is saved into ArrayList
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public static ArrayList<HashMap<String, String>> selectSQL(String query) throws Exception {
		ArrayList<HashMap<String, String>> list = new ArrayList<>();

		try {
			Connection connection = DBConnection.getDBConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(query);
					
			HashMap<String, String> map = null;

			while (resultSet.next()) {

				map = new HashMap<>();
				int columnCount =resultSet.getMetaData().getColumnCount();
				System.out.println("results table column count :: " + columnCount);
				for (int i = 1; i <= columnCount; i++) {
					String name = resultSet.getMetaData().getColumnName(i);
					String value = resultSet.getString(name);
					map.put(name,value);
					System.out.println("ColumnName  :: "+name+"  , Value  :: "+value );
				}

				list.add(map);
			}

		} catch (SQLException e) {
			DBConnection.closeConnection();
			e.printStackTrace();
			throw e;
		}

		return list;
	}
}