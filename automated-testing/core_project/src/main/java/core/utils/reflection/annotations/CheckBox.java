package core.utils.reflection.annotations;

/**
 * Created by okara on 10-Jan-17.
 */
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines the CheckBox Name that a given Page Object has.
 * This annotation lets you define a button name for a particular Page Object.
 * @author okara
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface CheckBox {
    String value();
}