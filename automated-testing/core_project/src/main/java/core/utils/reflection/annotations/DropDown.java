package core.utils.reflection.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines the DropDown Name that a given Page Object has.
 * This annotation lets you define a DropDown name for a particular Page Object.
 * @author okara
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DropDown {
    String value();
}