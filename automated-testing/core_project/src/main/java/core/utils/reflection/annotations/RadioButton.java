package core.utils.reflection.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines the RadioButton Name that a given Page Object has.
 * This annotation lets you define a RadioButton name for a particular Page Object.
 * @author olelysieev
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface RadioButton {
    String value();
}