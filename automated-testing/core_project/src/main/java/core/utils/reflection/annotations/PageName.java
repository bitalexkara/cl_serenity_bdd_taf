package core.utils.reflection.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines the Page Name that a given Page Object has.
 * This annotation lets you define a page name for a particular Page Object.
 * Generally takes from page's title.
 * @author okara
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PageName {
    String value();
}