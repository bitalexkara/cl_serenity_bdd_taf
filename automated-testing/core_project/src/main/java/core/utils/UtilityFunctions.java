package core.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
* This class contains reusable functions that should be shared between all entities
*/
public class UtilityFunctions {

	public static boolean isStringSet(String param) { 
	    // doesn't ignore spaces, but does save an object creation.
	    return param != null && param.length() != 0; 
	}
	
	public static String generateUniqueStringValue() {
		
		LocalDateTime ldt = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy_HH-mm-ss");
		String dateTime = ldt.format(formatter);
		
		return dateTime;
	}
	
	public static String getCurrentDate() {
		LocalDateTime ldt = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		
		return ldt.format(formatter);
	}
}