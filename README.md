# CL_SERENITY_BDD_TAF

* Details read [CONFLUENCE_CREDCOTSG-15240477-250319-1426-25.pdf][1]
* Video Overview: https://youtu.be/pda0hCQjpoo

# Preconditions

1. The `chromedriver.exe` should be correspondent to your Chrome installed version on Windows
	* make sure you have correct version of chromedriver in here: {project}/pom.xml <web.chrome.driver>
	* To download a driver: http://chromedriver.chromium.org/downloads
2. Make sure you have added and unziped the archive [1.38.1.zip][2] into here. [Pull Request][3] to Serenity has been done in the past: 
    * {user}\.m2\repository\net\serenity-bdd\serenity-jbehave\1.38.1\serenity-jbehave-1.38.1.jar
    * {user}\.m2\repository\net\serenity-bdd\serenity-jbehave\1.38.1\serenity-jbehave-1.38.1.pom
3. Install IDEA (Community works well)
4. Install IDEA plugin for story files: https://plugins.jetbrains.com/plugin/7268-jbehave-support

# How to run the scenario

1. Clone the repo
2. Import using IDEA (feel free to use Eclipse)
3. Create run configuration (IDEA)
	* Select working directory: /automated-testing/ebay_test_project
	* Command line: clean verify
	* Profiles: prod Chrome
	* Add before lunch step - Maven Goal
		* Working directory: /automated-testing/core_project
		* Command line: clean install
4. Find the test report: /automated-testing/ebay_test_project/target/site/serenity/index.html

[1]: CONFLUENCE_CREDCOTSG-15240477-250319-1426-25.pdf "PDF"
[2]: https://bitbucket.org/bitalexkara/cl_serenity_bdd_taf/src/master/1.38.1.zip "Zip"
[3]: https://github.com/serenity-bdd/serenity-jbehave/issues/186 "Pull Request"
